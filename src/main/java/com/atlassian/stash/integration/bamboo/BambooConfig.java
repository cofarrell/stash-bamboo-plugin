package com.atlassian.stash.integration.bamboo;

public class BambooConfig {

    private String plan;

    private BambooConfig() {
        super();
    }

    public BambooConfig(String plan) {
        this.plan = plan;
    }

    public String getPlan() {
        return plan;
    }
}
