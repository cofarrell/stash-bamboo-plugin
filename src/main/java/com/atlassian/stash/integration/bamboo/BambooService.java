package com.atlassian.stash.integration.bamboo;

import com.atlassian.applinks.api.*;
import com.atlassian.applinks.api.application.bamboo.BambooApplicationType;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.stash.pull.PullRequest;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.rest.util.NotFoundException;
import com.google.common.base.Function;
import com.google.common.base.Splitter;
import com.google.common.base.Supplier;
import com.google.common.collect.Iterables;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

import static com.atlassian.stash.integration.bamboo.util.ThrowableUtils.throwUnchecked;

public class BambooService {

    private static final String REST_RESULT_URL = "/rest/api/latest/result/%s?max-results=1&includeAllStates=true";
    private static final String REST_RESULT_BRANCH_URL = "/rest/api/latest/result/%s/branch/%s?max-results=1&includeAllStates=true";

    private final ApplicationLinkService applicationLinkService;
    private final BambooConfigService configService;

    public BambooService(ApplicationLinkService applicationLinkService, BambooConfigService configService) {
        this.applicationLinkService = applicationLinkService;
        this.configService = configService;
    }

    public boolean doesPlanExist(final String plan) {
        return getResults(new Supplier<String>() {
            @Override
            public String get() {
                return String.format(REST_RESULT_URL, plan);
            }
        }) != null;
    }

    public BambooResults getBuildStatus(Repository repository) {
        return getResults(repository, new Function<BambooConfig, String>() {
            @Override
            public String apply(BambooConfig config) {
                return String.format(REST_RESULT_URL, config.getPlan());
            }
        });
    }

    public BambooResults getBuildStatusPR(final PullRequest pullRequest) {
        return getResults(pullRequest.getFromRef().getRepository(), new Function<BambooConfig, String>() {
            @Override
            public String apply(BambooConfig config) {
                String branchName = pullRequest.getFromRef().getDisplayId().replace("refs/heads/", "");
                return String.format(REST_RESULT_BRANCH_URL, config.getPlan(), branchName);
            }
        });
    }

    public boolean create(PullRequest pullRequest) {
        ApplicationLink appLink = getAppLinkOrDie();
        BambooConfig config = configService.getConfig(pullRequest.getFromRef().getRepository());
        String branchName = pullRequest.getFromRef().getDisplayId().replace("refs/heads/", "");
        String path = "/chain/admin/createPlanBranch.action";
        ApplicationLinkRequest request = createRequest(appLink, path);
        request.addRequestParameters(
                "createOption", "AUTO",
                "branchesForCreation", branchName,
                "planKeyToClone", config.getPlan(),
                "planKey", config.getPlan(),
                "checkBoxFields", "tmp.createAsEnabled");

        try {
            return request.execute(isSuccessfulResponseHandler());
        } catch (ResponseException e) {
            throw throwUnchecked(e);
        }
    }

    public boolean delete(PullRequest pullRequest) {
        ApplicationLink appLink = applicationLinkService.getPrimaryApplicationLink(BambooApplicationType.class);
        if (appLink == null) {
            return false;
        }
        BambooConfig config = configService.getConfig(pullRequest.getFromRef().getRepository());
        if (config.getPlan().isEmpty()) {
            return false;
        }
        String path = "/chain/admin/deleteChain!doDelete.action";
        ApplicationLinkRequest request = createRequest(appLink, path);
        request.addRequestParameters(
                "returnUrl", "/",
                "save", "Confirm",
                "buildKey", config.getPlan());
        try {
            return request.execute(isSuccessfulResponseHandler());
        } catch (ResponseException e) {
            // Ignore
            return false;
        }
    }

    private static ApplicationLinkRequest createRequest(ApplicationLink appLink, String path) {
        try {
            return appLink.createAuthenticatedRequestFactory().createRequest(Request.MethodType.POST, path);
        } catch (CredentialsRequiredException e) {
            throw new RuntimeException(e);
        }
    }

    private static ApplicationLinkResponseHandler<Boolean> isSuccessfulResponseHandler() {
        return new ApplicationLinkResponseHandler<Boolean>() {
            @Override
            public Boolean credentialsRequired(com.atlassian.sal.api.net.Response response) throws ResponseException {
                return false;
            }

            @Override
            public Boolean handle(com.atlassian.sal.api.net.Response response) throws ResponseException {
                return response.isSuccessful();
            }
        };
    }

    private BambooResults getResults(final Repository repository, final Function<BambooConfig, String> path) {
        return getResults(new Supplier<String>() {
            @Override
            public String get() {
                BambooConfig config = configService.getConfig(repository);
                return !config.getPlan().isEmpty() ? path.apply(config) : null;
            }
        });
    }

    private BambooResults getResults(Supplier<String> path) {
        ApplicationLink appLink = getAppLinkOrDie();
        try {
            String path1 = path.get();
            return path1 != null ? updateLink(appLink, getResults(appLink, path1)) : null;
        } catch (CredentialsRequiredException e) {
            throw throwUnchecked(e);
        } catch (ResponseException e) {
            throw throwUnchecked(e);
        }
    }

    /**
     * Links are currently to api and not browse.
     */
    private static BambooResults updateLink(ApplicationLink appLink, BambooResults results) {
        if (results == null) {
            return results;
        }
        String url = appLink.getDisplayUrl() + "/browse/";
        for (BambooResult result : results.results.result) {
            result.link.href = url + result.key;
        }
        results.link.href = url + Iterables.getLast(Splitter.on("/").split(results.link.href));
        return results;
    }

    private BambooResults getResults(ApplicationLink appLink, String path) throws CredentialsRequiredException, ResponseException {
        ApplicationLinkRequest request = appLink.createAuthenticatedRequestFactory().createRequest(Request.MethodType.GET, path);
        return request.addHeader("Accept", "application/json").execute(new ApplicationLinkResponseHandler<BambooResults>() {
            @Override
            public BambooResults credentialsRequired(com.atlassian.sal.api.net.Response response) throws ResponseException {
                return null;
            }

            @Override
            public BambooResults handle(com.atlassian.sal.api.net.Response response) throws ResponseException {
                return response.isSuccessful() ? response.getEntity(BambooResults.class) : null;
            }
        });
    }

    private ApplicationLink getAppLinkOrDie() {
        ApplicationLink appLink = applicationLinkService.getPrimaryApplicationLink(BambooApplicationType.class);
        if (appLink == null) {
            throw new NotFoundException("No bamboo application link enabled");
        }
        return appLink;
    }

    public static enum BambooState {
        Unknown, Successful, Failed, InProgress
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class BambooResults {

        @JsonProperty
        public Link link;
        @JsonProperty
        public BambooResultList results;

    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class BambooResultList {

        @JsonProperty
        public List<BambooResult> result;

    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Link {
        @JsonProperty
        public String href;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class BambooResult {
        @JsonProperty
        private String state;
        @JsonProperty
        public String key;
        @JsonProperty
        private String lifeCycleState;
        @JsonProperty
        public Link link;

    }

}
