package com.atlassian.stash.integration.bamboo;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.stash.repository.Repository;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

public class BambooConfigService {

    private final PluginSettings pluginSettings;

    public BambooConfigService(PluginSettingsFactory pluginSettingsFactory) {
        this.pluginSettings = pluginSettingsFactory.createSettingsForKey("stash.bamboo");
    }

    private String getRepoKey(Repository repository) {
        return "build." + repository.getId();
    }

    public BambooConfig getConfig(Repository repository) {
        Map<String, String> config = (Map<String, String>) pluginSettings.get(getRepoKey(repository));
        return config != null ? new BambooConfig(config.get("plan")) : new BambooConfig("");
    }

    public void saveConfig(Repository repository, BambooConfig config) {
         pluginSettings.put(getRepoKey(repository), ImmutableMap.of("plan", config.getPlan()));
    }
}
