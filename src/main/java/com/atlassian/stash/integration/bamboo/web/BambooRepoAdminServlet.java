package com.atlassian.stash.integration.bamboo.web;

import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.integration.bamboo.BambooConfig;
import com.atlassian.stash.integration.bamboo.BambooConfigService;
import com.atlassian.stash.integration.bamboo.BambooService;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryService;
import com.atlassian.stash.user.Permission;
import com.atlassian.stash.user.PermissionValidationService;
import com.google.common.base.Function;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

public class BambooRepoAdminServlet extends HttpServlet {

    private final BambooConfigService bambooConfigService;
    private final BambooService bambooService;
    private final PermissionValidationService permissionValidationService;
    private final RepositoryService repositoryService;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final WebResourceManager webResourceManager;

    public BambooRepoAdminServlet(
            BambooConfigService bambooConfigService, BambooService bambooService, PermissionValidationService permissionValidationService,
            RepositoryService repositoryService, SoyTemplateRenderer soyTemplateRenderer,
            WebResourceManager webResourceManager) {
        this.bambooConfigService = bambooConfigService;
        this.bambooService = bambooService;
        this.permissionValidationService = permissionValidationService;
        this.repositoryService = repositoryService;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.webResourceManager = webResourceManager;
    }

    private <T> T handleRepository(HttpServletRequest req, HttpServletResponse resp, Function<Repository, T> chain) throws IOException {
        String pathInfo = req.getPathInfo();
        if (Strings.isNullOrEmpty(pathInfo) || pathInfo.equals("/")) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
        String[] pathParts = pathInfo.substring(1).split("/");
        if (pathParts.length != 2) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
        String projectKey = pathParts[0];
        String repoSlug = pathParts[1];
        Repository repository = repositoryService.findBySlug(projectKey, repoSlug);
        if (repository == null) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return null;
        }
        return chain.apply(repository);
    }

    @Override
    protected void doGet(HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        handleRepository(req, resp, new Function<Repository, Object>() {
            @Override
            public Object apply(Repository repository) {
                return doView(repository, resp, bambooConfigService.getConfig(repository));
            }
        });
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        handleRepository(req, resp, new Function<Repository, Object>() {
            @Override
            public Object apply(Repository repository) {
                BambooConfig config = new BambooConfig(req.getParameter("plan"));
                if (!bambooService.doesPlanExist(config.getPlan())) {
                    return doView(repository, resp, config, ImmutableMap.of("plan", (Collection<String>) Collections.singleton("Plan does not exist")));
                }
                bambooConfigService.saveConfig(repository, config);
                return doView(repository, resp, config);
            }
        });
    }

    private Object doView(Repository repository, HttpServletResponse resp, BambooConfig config) {
        return doView(repository, resp, config, ImmutableMap.<String, Collection<String>>of());
    }

    private Object doView(Repository repository, HttpServletResponse resp, BambooConfig config, Map<String, Collection<String>> fieldErrors) {
        permissionValidationService.validateForRepository(repository, Permission.REPO_ADMIN);
        try {
            render(resp,
                    "stash.page.bamboo.view",
                    ImmutableMap.<String, Object>builder()
                            .put("repository", repository)
                            .put("config", config)
                            .put("fieldErrors", fieldErrors)
                            .build()
            );
            return null;
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ServletException e) {
            throw new RuntimeException(e);
        }
    }

    private void render(HttpServletResponse resp, String templateName, Map<String, Object> data) throws IOException, ServletException {
        webResourceManager.requireResourcesForContext("plugin.page.bamboo");
        resp.setContentType("text/html;charset=UTF-8");
        try {
            soyTemplateRenderer.render(resp.getWriter(), "com.atlassian.stash.stash-bamboo-integration:bamboo-integration-serverside", templateName, data);
        } catch (SoyException e) {
            Throwable cause = e.getCause();
            if (cause instanceof IOException) {
                throw (IOException) cause;
            }
            throw new ServletException(e);
        }
    }
}
