package com.atlassian.stash.integration.bamboo.rest;

import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.stash.i18n.I18nService;
import com.atlassian.stash.integration.bamboo.BambooService;
import com.atlassian.stash.pull.PullRequest;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.rest.interceptor.ResourceContextInterceptor;
import com.atlassian.stash.rest.util.ResourcePatterns;
import com.atlassian.stash.rest.util.ResponseFactory;
import com.atlassian.stash.rest.util.RestResource;
import com.atlassian.stash.rest.util.RestUtils;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.stash.integration.bamboo.BambooService.BambooResults;

@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@InterceptorChain(ResourceContextInterceptor.class)
@Produces({RestUtils.APPLICATION_JSON_UTF8})
@Path("/")
@Singleton
public class BambooResource extends RestResource {

    private final BambooService bambooService;

    public BambooResource(I18nService i18nService, BambooService bambooService) {
        super(i18nService);
        this.bambooService = bambooService;
    }

    @GET
    @Path(ResourcePatterns.REPOSITORY_URI)
    public Response getBuildStatus(@Context Repository repository) {
        return getResults(bambooService.getBuildStatus(repository));
    }

    @GET
    @Path(ResourcePatterns.PULL_REQUEST_URI)
    public Response getBuildStatusPR(final @Context PullRequest pullRequest) {
        return getResults(bambooService.getBuildStatusPR(pullRequest));
    }

    @POST
    @Path(ResourcePatterns.PULL_REQUEST_URI)
    public Response createBranchBuild(@Context PullRequest pullRequest) {
        bambooService.create(pullRequest);
        return ResponseFactory.noContent().build();
    }

    private Response getResults(BambooResults results) {
        if (results == null) {
            // No branch build found
            return ResponseFactory.status(Response.Status.NO_CONTENT).build();
        } else {
            // There might be a branch build, but no results
            return ResponseFactory.ok().entity(results).build();
        }
    }

}
