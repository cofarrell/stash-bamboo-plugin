package com.atlassian.stash.integration.bamboo.rest;

import com.atlassian.stash.rest.exception.ResourceExceptionMapper;
import com.sun.jersey.spi.resource.Singleton;

import javax.ws.rs.ext.Provider;

@Provider
@Singleton
public class BambooResourceExceptionMapper extends ResourceExceptionMapper {
}
