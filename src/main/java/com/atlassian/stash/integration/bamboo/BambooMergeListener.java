package com.atlassian.stash.integration.bamboo;

import com.atlassian.event.api.EventListener;
import com.atlassian.stash.event.pull.PullRequestMergedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BambooMergeListener {

    private static final Logger log = LoggerFactory.getLogger(BambooMergeListener.class);

    private final BambooService bambooService;

    public BambooMergeListener(BambooService bambooService) {
        this.bambooService = bambooService;
    }

    @EventListener
    public void onMergeEvent(PullRequestMergedEvent event) {
        try {
            bambooService.delete(event.getPullRequest());
        } catch (Exception e) {
            log.error("Error deleting branch build", e);
        }
    }
}
