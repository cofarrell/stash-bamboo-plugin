package com.atlassian.stash.integration.bamboo.util;

public class ThrowableUtils {
    /**
     * Part A of a little compiler trick to throw checked exceptions without declaring them
     */
    public static RuntimeException throwUnchecked(Throwable e) {
        throw ThrowableUtils.<RuntimeException>throwAny(e);
    }

    /**
     * Part B of a little compiler trick to throw checked exceptions without declaring them
     */
    @SuppressWarnings("unchecked")
    private static <E extends Throwable> E throwAny(Throwable e) throws E {
        throw (E) e;
    }
}

