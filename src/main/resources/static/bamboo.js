define(
'integration/bamboo',
['exports', 'jquery', 'underscore', 'eve', 'aui', 'util/ajax', 'util/navbuilder', 'model/page-state'],
function(exports, $, _, eve, AJS, ajax, navBuilder, pageState) {

    function createState(result) {
        var state = result.lifeCycleState === 'InProgress' ? result.lifeCycleState : result.state;
        var $state = $("<a />");
        $state.attr('href', result.link.href);
        $state.attr('title', result.key + " is " + state);
        $state.addClass("state");
        return $state.addClass(state.toLowerCase());
    }

    function createBranchButton(onClick) {
        var $button = $("<a class='aui-button' />");
        $button.text("Branch Build").attr('title', 'Create a Bamboo Branch Build');
        $button.click(function() {
            $button.attr('disabled', true);
            onClick();
        });
        return $button;
    }

    function init($el, getUrl, emptyResult, prepend) {
        // Create an element for update which can be cleared
        // Need to be timeout for navbuilder
        var $status = $('<div class="bamboo-status aui-buttons"  />');
        !prepend ? $status.appendTo($el) : $status.prependTo($el);
        $(document).ready(_.bind(update, null, $status, getUrl, emptyResult));
    }

    function update($el, getUrl, emptyResult) {
        var self = function() {
            return update($el, getUrl, emptyResult);
        };
        ajax.rest({
            url: getUrl(),
            statusCode: {
                404: function() {return false;} // Do nothing
            }
        }).done(function(result) {
            if (!result) {
                var $empty = emptyResult(self);
                $el.empty();
                $empty && $el.append($empty);
                return;
            }
            $el.empty();
            // No results
            if (result.results.result.length === 0) {
                $el.append(createState({link: {href: result.link.href}, key: _.last(result.link.href.split('/')), state: "Empty"}));
            } else {
                _.each(result.results.result, function(result) {
                    $el.append(createState(result));
                    if(result.lifeCycleState === 'InProgress') {
                        // Keep reloading while in progress
                        setTimeout(self, 5000);
                    }
                });
            }
        });
    }

    var restUrl = AJS.contextPath() + '/rest/bamboo/latest';

    exports.init = function() {
        $(document).ready(function() {
            exports.initRepo();
            if (pageState.getPullRequest()) {
                exports.initPR();
            }
        });
    };

    exports.initRepo = function() {
        init($('.aui-page-header-actions'), function() {
            return restUrl + navBuilder.currentRepo()._path().buildRelNoContext();
        }, function(){}, true);
    };

    exports.initPR = function() {
        var getUrl = function() {
            return restUrl + navBuilder.currentPullRequest()._path().buildRelNoContext();
        };
        init($('.pull-request-toolbar .aui-toolbar2-secondary'), getUrl, function(reload) {
            return createBranchButton(function () {
                return ajax.rest({
                    url:getUrl(),
                    type:'POST'
                }).pipe(function () {
                        setTimeout(reload, 1000);
                    });
            });
        }, true);
    }
});
